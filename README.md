# File Image Formatter

File image formatter provides an image formatter for file fields.

This is most useful for web sites migrated from Drupal 6 with the default "upload" field from nodes that were used as a free-form file or image field.

## Usage

* Use when you need to display an image formatter in a view where a node has a file field with an image.
* Not as useful when used as a default display for a file field as non-image fields are hidden.

